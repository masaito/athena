#include "TrigVertexFitter/TrigVertexFitter.h"
#include "TrigVertexFitter/TrigPrimaryVertexFitter.h"
#include "TrigVertexFitter/TrigL2VertexFitter.h"
#include "TrigVertexFitter/TrigVertexingTool.h"

DECLARE_COMPONENT( TrigVertexFitter )
DECLARE_COMPONENT( TrigPrimaryVertexFitter )
DECLARE_COMPONENT( TrigL2VertexFitter )
DECLARE_COMPONENT( TrigVertexingTool )

